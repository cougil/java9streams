package com.netsuite.suitetalks;

import com.netsuite.suitetalks.model.Employee;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.netsuite.suitetalks.model.EmployeeBuilder.anEmployee;
import static java.util.stream.Collectors.toList;

public class EmployeesListDropWhile
{

	public static void main(String[] args) throws Exception
	{
		Options opt = new OptionsBuilder()
				.include(".*" + EmployeesListDropWhile.class.getSimpleName() + ".*")
				.warmupIterations(3)
				.measurementIterations(10)
				.build();
		new Runner(opt).run();
	}

	@Benchmark
	@BenchmarkMode(Mode.SingleShotTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public List<Employee> olderThan25_noStreams(BenchmarkState state)
	{

		List<Employee> employeesAbove35yo = new ArrayList<>();

		for (Employee employee : state.employees)
		{
			if (employee.getAge() >= 25) { employeesAbove35yo.add(employee); }
		}

		return employeesAbove35yo;
	}

	@Benchmark
	@BenchmarkMode(Mode.SingleShotTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public List<Employee> olderThan25_java8(BenchmarkState state)
	{
		return state.employees.stream()
							  .filter(employee -> employee.getAge() >= 25)
							  .collect(toList());
	}

	@Benchmark
	@BenchmarkMode(Mode.SingleShotTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public List<Employee> olderThan25_java9(BenchmarkState state)
	{
		return state.employees.stream()
							  .dropWhile(employee -> employee.getAge() < 25)
							  .collect(toList());
	}

	@State(Scope.Benchmark)
	public static class BenchmarkState
	{
		List<Employee> employees = List.of(
				anEmployee().withAge(24).withName("Tom").build(),
				anEmployee().withAge(29).withName("Billy").build(),
				anEmployee().withAge(30).withName("Dan").build(),
				anEmployee().withAge(31).withName("George").build(),
				anEmployee().withAge(32).withName("Ken").build(),
				anEmployee().withAge(33).withName("Mike").build(),
				anEmployee().withAge(36).withName("Jason").build(),
				anEmployee().withAge(36).withName("Keith").build()
		);
	}
}
