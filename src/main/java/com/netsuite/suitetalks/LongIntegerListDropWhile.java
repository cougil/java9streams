package com.netsuite.suitetalks;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class LongIntegerListDropWhile
{

	public static void main(String[] args) throws Exception
	{
		Options opt = new OptionsBuilder()
				.include(".*" + LongIntegerListDropWhile.class.getSimpleName() + ".*")
				.warmupIterations(3)
				.measurementIterations(4)
				.forks(3)
				.build();
		new Runner(opt).run();
	}

	@Benchmark
	@BenchmarkMode(Mode.Throughput)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public List<Integer> getGreaterThan5InA1to10000List_noStreams(BenchmarkState state)
	{
		List<Integer> greaterThan5 = new ArrayList<>();

		for (Integer integer : state.sortedIntegerList)
		{
			if (integer > 5)
			{
				greaterThan5.add(integer);
			}
		}

		return greaterThan5;
	}

	@Benchmark
	@BenchmarkMode(Mode.Throughput)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public List<Integer> getGreaterThan5InA1to10000List_java8(BenchmarkState state)
	{
		return state.sortedIntegerList.stream()
									  .filter(i -> i > 5)
									  .collect(toList());
	}

	@Benchmark
	@BenchmarkMode(Mode.Throughput)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public List<Integer> getGreaterThan5InA1to10000List_java9(BenchmarkState state)
	{
		return state.sortedIntegerList.stream()
									  .dropWhile(i -> i <= 5)
									  .collect(toList());
	}

	@Benchmark
	@BenchmarkMode(Mode.Throughput)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public List<Integer> getGreaterThan10000InA1to10000List_noStreams(BenchmarkState state)
	{
		List<Integer> greaterThan10000 = new ArrayList<>();

		for (Integer integer : state.sortedIntegerList)
		{
			if (integer > 10000)
			{
				greaterThan10000.add(integer);
			}
		}

		return greaterThan10000;
	}

	@Benchmark
	@BenchmarkMode(Mode.Throughput)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public List<Integer> getGreaterThan10000InA1to10000List_java8(BenchmarkState state)
	{
		return state.sortedIntegerList.stream()
									  .filter(i -> i > 10000)
									  .collect(toList());
	}

	@Benchmark
	@BenchmarkMode(Mode.Throughput)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public List<Integer> getGreaterThan10000InA1to10000List_java9(BenchmarkState state)
	{
		return state.sortedIntegerList.stream()
									  .dropWhile(i -> i <= 10000)
									  .collect(toList());
	}

	@State(Scope.Benchmark)
	public static class BenchmarkState
	{
		List<Integer> sortedIntegerList = IntStream.range(0, 1000).boxed().collect(toList());
	}
}
